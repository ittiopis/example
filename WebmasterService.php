<?php

namespace Common\Main\Service;

use Common\Main\Entity\SubSite\Site;
use Common\Main\Interfaces\WebmasterInterface;
use Doctrine\ORM\EntityManager;

class WebmasterService implements WebmasterInterface
{
    /**
     * @var WebmasterInterface[]
     */
    protected $webmasters;

    /**
     * @var EntityManager
     */
    public $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param WebmasterInterface $webmaster
     */
    public function addWebmaster(WebmasterInterface $webmaster)
    {
        $this->webmasters[] = $webmaster;
    }

    /**
     * @inheritdoc
     */
    public function add(Site $site)
    {
        $result = true;
        foreach ($this->webmasters as $webmaster) {
            $result &= $webmaster->add($site);
        }

        return $result;
    }

    /**
     * @param Site $site
     * @return bool
     */
    public function remove(Site $site)
    {
        $result = true;
        foreach ($this->webmasters as $webmaster) {
            $result &= $webmaster->remove($site);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function verify(Site $site)
    {
        $result = true;
        foreach ($this->webmasters as $webmaster) {
            $result &= $webmaster->verify($site);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function check(Site $site)
    {
        $result = true;
        foreach ($this->webmasters as $webmaster) {
            $result &= $webmaster->check($site);
        }

        return $result;
    }

    public function pullWebmasterSites()
    {
        foreach ($this->webmasters as $webmaster) {
            $webmaster->pullWebmasterSites();
        }
    }

    public function fillVerificationStatuses()
    {
        foreach ($this->webmasters as $webmaster) {
            $webmaster->fillVerificationStatuses();
        }
    }
}
