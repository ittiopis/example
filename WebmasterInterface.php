<?php

namespace Common\Main\Interfaces;

use Common\Main\Entity\SubSite\Site;

interface WebmasterInterface
{
    const MAX_SITE_REQUEST_ERRORS_COUNT = 20;

    /**
     * @param Site $site
     * @return bool
     */
    public function add(Site $site);

    /**
     * @param Site $site
     * @return bool
     */
    public function remove(Site $site);

    /**
     * @param Site $site
     * @return bool
     */
    public function verify(Site $site);

    /**
     * @param Site $site
     * @return bool
     */
    public function check(Site $site);

    /**
     * @return void
     */
    public function pullWebmasterSites();

    /**
     * @return void
     */
    public function fillVerificationStatuses();
}
