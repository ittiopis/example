<?php

namespace Common\Main\Service;

use Doctrine\ORM\EntityManager;
use Common\Base\Options\ProjectHostsOptions;
use Common\Main\Entity\SubSite\GoogleServiceAccount;
use Common\Main\Entity\SubSite\GoogleVerificationStatus;
use Common\Main\Repository\SubSite\GoogleServiceAccountRepository;
use Common\Main\Repository\SubSite\SiteRepository;
use Common\Main\Interfaces\WebmasterInterface;
use Common\Main\Service\Webmaster\GoogleWmServiceTable;
use Common\Main\Entity\SubSite\Site;
use Zend\Log\LoggerInterface;

class GoogleWebmasterService implements WebmasterInterface
{
    public const FULL_COUNT = 1000;

    /** @var GoogleWebmasterApi */
    private $webmaster;

    /** @var string */
    private $token;

    /** @var string */
    private $configDir;

    /** @var EntityManager */
    private $entityManager;

    /** @var ProjectHostsOptions */
    private $projectHosts;

    /** @var LoggerInterface */
    private $errorLogger;

    /** @var GoogleWmServiceTable */
    private $serviceTable;

    /** @var Site|null */
    protected $lastSiteFromMassRequests;

    public function __construct(
        EntityManager $entityManager,
        ProjectHostsOptions $projectHostsOptions,
        string $token,
        LoggerInterface $errorLogger,
        string $configDir
    ) {
        $this->entityManager = $entityManager;
        $this->projectHosts = $projectHostsOptions;
        $this->token = $token;
        $this->errorLogger = $errorLogger;
        $this->configDir = $configDir;
        $this->serviceTable = new GoogleWmServiceTable($entityManager);
    }

    /**
     * @param Site $site
     * @return bool
     */
    public function add(Site $site)
    {
        $siteUrl = $this->getSiteUrl($site);

        try {
            /** @var GoogleWebmasterApi $api */
            /** @var GoogleServiceAccount $serviceAccount */
            [$serviceAccount, $api] = $this->buildNotFullServiceAccount();

            $api->addSite($siteUrl);

            $verificationStatus = $site->getGoogleVerificationStatus() ?? new GoogleVerificationStatus();
            $verificationStatus->setAccount($serviceAccount);

            if (!$verificationStatus->getId()) {
                $verificationStatus->setSite($site);
                $this->entityManager->persist($verificationStatus);
            }

            $this->entityManager->flush();
            $this->entityManager->refresh($site);
        } catch (\Exception $e) {
            $this->errorHandler($e);

            return false;
        }

        return true;
    }

    /**
     * @param Site $site
     * @return bool
     */
    public function remove(Site $site)
    {
        $api = $this->buildApiFromSite($site);

        if (!$api) {
            return true;
        }

        $siteUrl = $this->getSiteUrl($site);

        try {
            $api->delSite($siteUrl);

            $verificationStatus = $site->getGoogleVerificationStatus();
            $account = $verificationStatus->getAccount();

            $account->setIsFull(false);
            $this->entityManager->remove($verificationStatus);

            $this->entityManager->flush();
        } catch (\Exception $e) {
            $this->errorHandler($e);

            return false;
        }

        return true;
    }

    /**
     * @param Site $site
     * @return bool
     */
    public function verify(Site $site)
    {
        $verifier = $this->buildVerifierFromSite($site);

        if (!$verifier) {
            return false;
        }

        $verificationStatus = $site->getGoogleVerificationStatus();
        $siteUrl = $this->getSiteUrl($site);

        try {
            if (empty($verificationStatus->getVerificationCode())) {
                $token = $verifier->getToken($siteUrl, $verifier::META);

                $verificationStatus->setVerificationCode($token);

                $this->entityManager->flush($verificationStatus);
            }

            $verifier->verify($siteUrl, $verifier::META);
        } catch (\Exception $e) {
            $this->errorHandler($e);

            return false;
        }

        return true;
    }

    /**
     * @param Site $site
     * @return string
     */
    public function check(Site $site)
    {
        $api = $this->buildApiFromSite($site);

        if (!$api) {
            return false;
        }

        $siteUrl = $this->getSiteUrl($site);

        try {
            $wmSite = $api->getInfo($siteUrl);

            return $wmSite->permissionLevel === 'siteOwner';
        } catch (\Exception $e) {
            $this->errorHandler($e);

            return false;
        }
    }

    /**
     * @param \Google_Client $client
     * @return \Google_Service_Webmasters_WmxSite
     */
    public function getSiteList(\Google_Client $client)
    {
        $this->webmaster = new GoogleWebmasterApi($client);

        $sites = $this->webmaster->listSites();

        $list = $sites->getSiteEntry();

        return $list;
    }

    protected function getSiteUrl(Site $site)
    {
        return $this->projectHosts->getPagesHost($site->getAddress());
    }

    protected function buildVerifierFromSite(Site $site)
    {
        $verification = $site->getGoogleVerificationStatus();

        if (!$verification) {
            return null;
        }

        return $this->buildVerifier($verification->getAccount());
    }

    public function buildVerifier(GoogleServiceAccount $account)
    {
        $client =  $this->buildClient($account->getName());

        return new GoogleWebmasterVerifier($client);
    }

    /**
     * @param Site $site
     * @return null|GoogleWebmasterApi
     */
    protected function buildApiFromSite(Site $site)
    {
        $verification = $site->getGoogleVerificationStatus();

        if (!$verification) {
            return null;
        }

        return $this->buildApi($verification->getAccount());
    }

    public function buildApi(GoogleServiceAccount $account)
    {
        $client =  $this->buildClient($account->getName());

        return new GoogleWebmasterApi($client);
    }

    /**
     * @param string $filename
     * @return \Google_Client
     */
    protected function buildClient(string $filename)
    {
        $client = new \Google_Client();
        $client->setAuthConfig($this->configDir . $filename);
        $client->setScopes([
            \Google_Service_Webmasters::WEBMASTERS,
            \Google_Service_SiteVerification::SITEVERIFICATION_VERIFY_ONLY
        ]);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('auto');
        $client->setAccessToken($this->token);

        return $client;
    }

    /**
     * @return array
     */
    private function buildNotFullServiceAccount()
    {
        /** @var GoogleServiceAccountRepository $GoogleServiceAccountRepository */
        $GoogleServiceAccountRepository = $this->entityManager->getRepository(GoogleServiceAccount::class);

        $found = false;

        while (!$found) {
            /** @var GoogleServiceAccount $account */
            $account = $GoogleServiceAccountRepository->findOneBy(['isFull' => false]);

            if (!$account) {
                throw new \RuntimeException('There are no available service accounts!');
            }

            $api = $this->buildApi($account);

            if (count($api->listSites()->getSiteEntry()) >= self::FULL_COUNT) {
                $account->setIsFull(true);
                $this->entityManager->flush($account);
            } else {
                $found = true;
            }
        }

        return [$account, $api];
    }

    public function pullWebmasterSites()
    {
        $this->serviceTable->clear();

        /** @var GoogleServiceAccountRepository $GoogleServiceAccountRepository */
        $GoogleServiceAccountRepository = $this->entityManager->getRepository(GoogleServiceAccount::class);

        /** @var GoogleServiceAccount $account */
        foreach ($GoogleServiceAccountRepository->findAll() as $account) {
            $api = $this->buildApi($account);
            $response = $api->listSites()->getSiteEntry();
            foreach ($response as $site) {
                if (empty($site)) {
                    continue;
                }

                $this->serviceTable->insertRow($account->getId(), $site['siteUrl'], $site['permissionLevel']);
            }
        }

        $this->serviceTable->associateWithSites();
    }

    public function fillVerificationStatuses()
    {
        $this->lastSiteFromMassRequests = null;

        $wmSiteWithoutSites = $this->serviceTable->findWmSiteWithoutSites();
        if ($wmSiteWithoutSites) {
            $accountRepository = $this->entityManager->getRepository(GoogleServiceAccount::class);
            $account = null;
            foreach ($wmSiteWithoutSites as $wmSite) {
                /** @var GoogleServiceAccount $account */
                if (!$account || $account->getId() != $wmSite['account_id']) {
                    $account = $accountRepository->find($wmSite['account_id']);
                }

                try {
                    $api = $this->buildApi($account);
                    $api->delSite($wmSite['site_url']);
                } catch (\Exception $e) {
                    $this->errorHandler($e);
                }
            }
        }

        /** @var SiteRepository $siteRepository */
        $siteRepository = $this->entityManager->getRepository(Site::class);

        $notAddedSites = $siteRepository->findWithoutVerificationStatus(GoogleVerificationStatus::class);
        if ($notAddedSites) {
            /** @var Site $site */
            foreach ($notAddedSites as $site) {
                $this->lastSiteFromMassRequests = $site;

                if ($site->getErrorCountOnMassRequestsGoogle() <= self::MAX_SITE_REQUEST_ERRORS_COUNT) {
                    $this->add($site);
                }
            }
        }

        $notVerifiedSites = $siteRepository->findNotVerified(GoogleVerificationStatus::class);
        if ($notVerifiedSites) {
            /** @var Site $site */
            foreach ($notVerifiedSites as $site) {
                $this->lastSiteFromMassRequests = $site;

                if ($site->getErrorCountOnMassRequestsGoogle() <= self::MAX_SITE_REQUEST_ERRORS_COUNT) {
                    $this->verify($site);
                }
            }
        }
    }

    protected function errorHandler(\Exception $e)
    {
        $site = $this->lastSiteFromMassRequests;

        if ($site) {
            $site->setErrorCountOnMassRequestsGoogle($site->getErrorCountOnMassRequestsGoogle() + 1);
            $this->entityManager->flush($site);
        } else {
            $this->errorLogger->err($e->getMessage());
        }
    }
}
